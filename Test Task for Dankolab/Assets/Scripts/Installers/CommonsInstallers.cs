using Commons;
using Zenject;

namespace Installers
{
    public class CommonsInstallers : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<UIHandler>().AsSingle();
            Container.Bind<CheckGame>().AsSingle();
            Container.Bind<ColorSquares>().AsSingle();
        }
    }
}