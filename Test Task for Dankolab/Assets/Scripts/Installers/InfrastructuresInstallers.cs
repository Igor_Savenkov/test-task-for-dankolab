using Zenject;
using Infrastructures;

namespace Installers
{
    public class InfrastructuresInstallers : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<IStaticDataProvider>().To<StaticDataProvider>().AsSingle();
            Container.BindInterfacesTo<InitializationProviders>().AsSingle().NonLazy();
        }
    }
}