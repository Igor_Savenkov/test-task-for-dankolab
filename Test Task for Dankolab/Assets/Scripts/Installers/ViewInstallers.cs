using UnityEngine;
using Views;
using Zenject;

namespace Installers
{
    public class ViewInstallers : MonoInstaller
    {
        [Header("Settings Views")]
        [SerializeField] private SquareView _squareView;
        [SerializeField] private GridSquareView _gridSquareView;
        [SerializeField] private WindowsView _windowsView;

        public override void InstallBindings()
        {
            Container.Bind<SquareView>().FromInstance(_squareView);
            Container.Bind<GridSquareView>().FromInstance(_gridSquareView);
            Container.Bind<WindowsView>().FromInstance(_windowsView);
        }
    }
}