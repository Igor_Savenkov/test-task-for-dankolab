using System.Collections.Generic;
using Views;
using Zenject;

namespace Commons
{
    public class CheckGame
    {
        private List<SquareView> _squareViews = new List<SquareView>();
        private WindowsView _windowsView;

        private bool IsCheckWin()
        {
            foreach (var view in _squareViews)
                if (view.IsCheckNumberCorrect() == false)
                    return false;

            return true;
        }

        [Inject]
        public void Construct(WindowsView windowsView)
        {
            _windowsView = windowsView;
        }

        public void SetListSquareViews(List<SquareView> squareViews)
        {
            _squareViews.Clear();
            _squareViews = squareViews;
        }

        public void Check()
        {
            if (IsCheckWin() == true)
                _windowsView.DoWin();
            else
                _windowsView.DoLose();
        } 
    }
}