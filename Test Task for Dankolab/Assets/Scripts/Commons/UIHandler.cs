using Views;
using Zenject;

namespace Commons
{
    public class UIHandler
    {
        private SquareView _squareViewTemp;
        private ColorSquares _colorSquares;

        private int columnTemp, rowTemp;
        private int currentNumber = 0;

        [Inject]
        public void Construct(ColorSquares colorSquares)
        {
            _colorSquares = colorSquares;
        }

        public void SetSquareView(SquareView squareView, int column, int row, int indexSector)
        {
            columnTemp = column;
            rowTemp = row;
            _squareViewTemp = squareView;
            _colorSquares.ResetSquaresColor();
            _colorSquares.SetSquareColorRowsAndColumnsAndSector(columnTemp, rowTemp, indexSector);
        }

        public void SetNumber(int number)
        {
            currentNumber = number;

            if (_squareViewTemp != null && currentNumber > 0)
            {
                _squareViewTemp.SetNumber(currentNumber);
                _colorSquares.ResetSquaresColorRed();
                _colorSquares.SetSquareColorToSameNumbers(_squareViewTemp, currentNumber, columnTemp, rowTemp);
                currentNumber = 0;
            }
        }
    }
}