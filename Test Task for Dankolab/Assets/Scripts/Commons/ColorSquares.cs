using System.Collections.Generic;
using Views;
using UnityEngine;

namespace Commons
{
    public class ColorSquares
    {
        private List<SquareView> _squareView = new List<SquareView>();

        public void SetSquaresView(List<SquareView> squareView)
        {
            _squareView.Clear();
            _squareView = squareView;
        }

        public void ResetSquaresColor()
        {
            foreach (var squareView in _squareView)
                squareView.SetBackImageColor(Color.white);
        }

        public void ResetSquaresColorRed()
        {
            foreach (var squareView in _squareView)
                if (squareView.GetColor() == Color.red)
                    squareView.SetBackImageColor(Color.cyan);
        }

        public void SetSquareColorRowsAndColumnsAndSector(int column, int row, int indexSector)
        {
            foreach (var squareView in _squareView)
                if (squareView.Column == column || squareView.Row == row || squareView.IndexSector == indexSector)
                    squareView.SetBackImageColor(Color.cyan);
        }

        public void SetSquareColorToSameNumbers(SquareView squareViewCurrent, int number, int column, int row)
        {
            foreach (var squareView in _squareView)
                if (((squareView.Column == column && number == squareView.Number) ||
                    (squareView.Row == row && number == squareView.Number)) &&
                    squareView != squareViewCurrent)
                    squareView.SetBackImageColor(Color.red);
        }
    }
}