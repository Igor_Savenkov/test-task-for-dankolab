using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Infrastructures;
using Commons;

namespace Views
{
    public class GridSquareView : MonoBehaviour
    {
        [Header("Settings Grid")]
        [SerializeField][Min(0)] private float squareOffset;

        private int columns = 9;
        private int rows = 9;
        private int indexSector = 0, indexSectorRow = 0;

        private GameDifficulty _gameDifficulty = GameDifficulty.Easy;
        private IStaticDataProvider _staticDataProvider;
        private SquareView _squareView;
        private SquareView _squareViewTemp;
        private List<SquareView> _squareViews = new List<SquareView>();
        private UIHandler _uIHandler;
        private CheckGame _checkGame;
        private ColorSquares _colorSquares;

        [Inject]
        public void Construct(SquareView squareView, 
                              IStaticDataProvider staticDataProvider, 
                              UIHandler uIHandler, 
                              CheckGame checkGame, 
                              ColorSquares colorSquares)
        {
            _checkGame = checkGame;
            _squareView = squareView;
            _staticDataProvider = staticDataProvider;
            _uIHandler = uIHandler;
            _colorSquares = colorSquares;
        }

        private void Start()
        {
            SpawnSquares();
        }

        public void SpawnSquares()
        {
            int countSquares = 0;

            for (int i = 0; i < rows; i++)
            {
                CalculateIndexSectorRow(i);

                for (int j = 0; j < columns; j++)
                {
                    CalculateIndexSector(j);

                    _squareViewTemp = Instantiate(_squareView, transform.position, Quaternion.identity, transform);
                    _squareViews.Add(_squareViewTemp);
                    SetUIHandlerToSquareView(_squareViewTemp);
                    SetSquareNumber(_squareViewTemp, countSquares);
                    SetPositionSquare(_squareViewTemp, i, j);
                    SetColumnAndRowToSquareView(_squareViewTemp, i, j, indexSector);
                    countSquares++;
                }
            }

            TransferListToCheckGame();
            _colorSquares.SetSquaresView(_squareViews);
        }

        private void CalculateIndexSectorRow(int row)
        {
            if (row % 3 == 0)
                indexSectorRow++;
            if (indexSectorRow > 3)
                indexSectorRow = 1;
        }

        private void CalculateIndexSector(int column)
        {
            if (column % 3 == 0)
                indexSector++;
            if (indexSector > 3 * indexSectorRow)
                indexSector -= 3;
        }

        private void TransferListToCheckGame()
        {
            _checkGame.SetListSquareViews(_squareViews);
        }

        public void ResetCurrentGrid()
        {
            indexSector = 0;
            indexSectorRow = 0;

            foreach (var squareView in _squareViews)
                Destroy(squareView.gameObject);

            _squareViews.Clear();
            _squareViews = new List<SquareView>();
        }

        private void SetUIHandlerToSquareView(SquareView squareView)
        {
            squareView.SetUIHandler(_uIHandler);
        }

        private void SetPositionSquare(SquareView squareView, int columns, int rows)
        {
            var squareRect = squareView.GetComponent<RectTransform>();

            squareRect.anchoredPosition = new Vector2((squareRect.rect.width * squareRect.transform.localScale.x + squareOffset) * columns, (squareRect.rect.height * squareRect.transform.localScale.y + squareOffset) * rows);
        }

        private void SetColumnAndRowToSquareView(SquareView squareView, int column, int row, int indexSector)
        {
            squareView.SetColumnAndRowAndSector(column, row, indexSector);
        }

        public void SetGameDifficulty(GameDifficulty gameDifficulty)
        {
            _gameDifficulty = gameDifficulty;
        }

        private void SetSquareNumber(SquareView squareView, int index)
        {
            squareView.SetCorrectNumber(_staticDataProvider.GetData(_gameDifficulty).dataSolved[index]);
            squareView.SetNumber(_staticDataProvider.GetData(_gameDifficulty).dataUnsolved[index], false, true);
        }
    }
}