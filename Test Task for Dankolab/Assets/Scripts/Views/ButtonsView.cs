using Commons;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Views
{
    public class ButtonsView : MonoBehaviour
    {
        private GridSquareView _gridSquareView;
        private CheckGame _checkGame;

        [Inject]
        public void Construct(GridSquareView gridSquareView, 
                              CheckGame checkGame)
        {
            _gridSquareView = gridSquareView;
            _checkGame = checkGame;
        }

        public void SetGameDifficulty(int gameDifficulty)
        {
            _gridSquareView.ResetCurrentGrid();
            _gridSquareView.SetGameDifficulty((GameDifficulty)gameDifficulty);
            _gridSquareView.SpawnSquares();
        }

        public void CheckGame()
        {
            _checkGame.Check();
        }

        public void Restart()
        {
            SceneManager.LoadScene(0);
        }
    }
}