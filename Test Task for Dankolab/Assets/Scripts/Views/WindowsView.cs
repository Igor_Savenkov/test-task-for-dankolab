using UnityEngine;

namespace Views
{
    public class WindowsView : MonoBehaviour
    {
        [Header("Settings Windows")]
        [SerializeField] private GameObject winWindow;
        [SerializeField] private GameObject loseWindow;

        public void DoLose()
        {
            ActivateWindow(loseWindow);
        }

        public void DoWin()
        {
            ActivateWindow(winWindow);
        }

        private void ActivateWindow(GameObject window)
        {
            window.SetActive(true);
        }
    }
}