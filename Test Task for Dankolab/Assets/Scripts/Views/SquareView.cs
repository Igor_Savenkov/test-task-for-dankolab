using Commons;
using TMPro;
using TMPro.EditorUtilities;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Views
{
    public class SquareView : MonoBehaviour, IPointerClickHandler
    {
        [Header("Settings Number")]
        [SerializeField] private Image backImage;
        [SerializeField] private TextMeshProUGUI numberText;

        private int row, column, indexSector;
        private bool isCanChangeNumber = false;
        private int number, correctNumber;
        private UIHandler _uIHandler;

        public int Column { get => column; }
        public int Row { get => row; }
        public int Number { get => number; }
        public int IndexSector { get => indexSector; }

        public bool IsCheckNumberCorrect()
        {
            if (number == correctNumber)
                return true;
            else 
                return false;
        }

        public Color GetColor()
        {
            return backImage.color;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            TransferSquareView();
        }

        public void SetUIHandler(UIHandler uIHandler)
        {
            _uIHandler = uIHandler;
        }

        private void TransferSquareView()
        {
            if (isCanChangeNumber == true)
                _uIHandler.SetSquareView(this, column, row, indexSector);
        }

        public void SetBackImageColor(Color color)
        {
            if (backImage.color == Color.red && isCanChangeNumber == true && number != correctNumber)
                return;

            backImage.color = color;
        }

        public void SetCorrectNumber(int number)
        {
            correctNumber = number;
        }

        public void SetColumnAndRowAndSector(int column, int row, int indexSector)
        {
            this.column = column;
            this.row = row;
            this.indexSector = indexSector;
        }

        private void SetNumberIsCanChange()
        {
            isCanChangeNumber = true;
        }

        public void SetNumber(int number, bool isNeedCheck = true, bool isCanChange = false)
        {
            this.number = number;
            DisplayNumberText();
            if (isNeedCheck == true)
                CheckNumberIsCorrect();
            if (isCanChange == true && number <= 0)
                SetNumberIsCanChange();
        }

        private void DisplayNumberText()
        {
            if (number <= 0)
                numberText.text = " ";
            else
                numberText.text = $"{number}";
        }

        private void CheckNumberIsCorrect()
        {
            if (IsCheckNumberCorrect() == true)
                SetBackImageColor(Color.green);
            else
                SetBackImageColor(Color.red);

        }
    }
}