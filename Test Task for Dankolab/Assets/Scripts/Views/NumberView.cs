using Commons;
using UnityEngine;
using Zenject;

namespace Views
{
    public class NumberView : MonoBehaviour
    {
        [Header("Settings Number")]
        [SerializeField][Range(1,9)] private int number;

        private UIHandler _uIHandler;

        [Inject]
        public void Construct(UIHandler uIHandler)
        {
            _uIHandler = uIHandler;
        }

        public void TransferNumberToUIHandler()
        {
            _uIHandler.SetNumber(number);
        }
    }
}