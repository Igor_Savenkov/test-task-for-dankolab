using UnityEngine;

public enum GameDifficulty { Easy, Medium, Hard }

namespace Configs
{
    [CreateAssetMenu(menuName = "Static Data/Item static data", fileName = "SudokuData")]
    public class SudokuData : ScriptableObject
    {
        public GameDifficulty gameDifficulty;
        [Range(0, 9)] public int[] dataSolved = new int[81];
        [Range(0, 9)] public int[] dataUnsolved = new int[81];
    }
}