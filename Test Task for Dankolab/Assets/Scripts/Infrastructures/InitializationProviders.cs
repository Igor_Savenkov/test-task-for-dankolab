using UnityEngine;
using Zenject;

namespace Infrastructures
{
    public class InitializationProviders : IInitializable
    {
        private IStaticDataProvider _staticDataProvider;

        [Inject]
        public InitializationProviders(IStaticDataProvider staticDataProvider)
        {
            _staticDataProvider = staticDataProvider;
        }

        public void Initialize()
        {
            _staticDataProvider.Load();
        }
    }
}