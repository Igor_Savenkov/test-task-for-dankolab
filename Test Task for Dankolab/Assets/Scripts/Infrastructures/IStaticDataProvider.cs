using Configs;

namespace Infrastructures
{
    public interface IStaticDataProvider
    {
        void Load();
        SudokuData GetData(GameDifficulty gameDifficulty);
    }
}