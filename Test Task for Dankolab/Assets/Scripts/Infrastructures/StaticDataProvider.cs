using System;
using System.Collections.Generic;
using System.Linq;
using Configs;
using UnityEngine;

namespace Infrastructures
{
    public class StaticDataProvider : IStaticDataProvider
    {
        private const string ItemsStaticDataKey = "StaticData/SudokuData";

        private Dictionary<GameDifficulty, SudokuData> sudokoDataset = new Dictionary<GameDifficulty, SudokuData>();

        public void Load()
        {
            sudokoDataset = Resources
              .LoadAll<SudokuData>(ItemsStaticDataKey)
              .ToDictionary(x => x.gameDifficulty, x => x);
        }

        public SudokuData GetData(GameDifficulty gameDifficulty)
        {
            if (sudokoDataset.TryGetValue(gameDifficulty, out var itemStaticData))
                return itemStaticData;

            throw new Exception($"item {gameDifficulty} not found");
        }
    }
}